# EmLi

The main goal of the project **EmLi** (Email List) is sending emails to several
users with some time delay to avoid spam problem. Here, the basic libraries
of the programming language Python together with **GTK** visual toolkit are used
to provide end user with minimal graphical user interface.

## Project structure

The given project has two main packages namely **emliGlobal** and ***emliLocal**.
The first package is aimed at installing software globally using PyPi repository.
The second package is possible to use locally from the same directory without global
installation. The difference in the code of both packages is small and only concerns
variable renaming.

Both packages consists of

1. **emli.py** is the main file which starts software initiation
2. **alltypes.py** is file which contains basic constants and functions for multiple usage
3. **mail.py** implements sending electronic letter from one mail to another mail
4. **reader.py** contains functions to read different types of text files for configuration
5. **gui.py** is module to organize graphical user interface for sending configuration using PyGTK library
6. **__init__.py** is special code file to determine initial values
7. **emli.glade** is XML-file created by special GTK rules to specify user interface


## Interface

The program interface consists of the button, file chooser entry, file chooser system and big entry field
with the possibility of text marking. The file chooser entry object is created for entering a tabular csv-file
containing information about the destination e-mails where the letters should arrive. The file chooser system
gives possibility

<img src="https://gitlab.com/bridgearchitect/emli/-/blob/main/figure/interface.png?ref_type=heads" width="60%"/>

## Execution

There are two possible algorithms for installing this software on your own computer. The first algorithm is
local installation. Its essence is to download this repository and go locally to the directory ***emliLocal**.
After that, you need to run the following command:

```bash
python3 emli.py
```

The second way is installation using PyPi repository ([URL address](https://pypi.org/project/EmLiLetter/)). In
this case, the next command in terminal should be written:

```bash
pip install EmLiLetter
```

With the given selected option, program files from directory **emliGlobal** will be installed. Launching the
application after installation will be possible with a simple command:

```bash
emli
```

## License

This project is distributed under a license MIT.
