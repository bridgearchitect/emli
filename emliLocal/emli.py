# configure local environment
import sys
import os
# configure system path
sys.path.append(os.getcwd())

# import local libraries
import alltypes
import gui

# entry point for EmLi package
def main():
    # debug code
    entryFilePath = os.path.dirname(os.path.abspath(__file__))
    # create builder
    builder = gui.createBuilder(os.path.join(entryFilePath, alltypes.filenameBuilder))
    # create main GUI objects
    mainWindow = gui.receiveObject(builder, alltypes.nameMainWindow)
    mailButton = gui.receiveObject(builder, alltypes.nameMailButton)
    # create file chooser objects
    dataFileChooser = gui.receiveObject(builder, alltypes.nameDataFileChooser)
    # create text entry field objects
    subjectEnrty = gui.receiveObject(builder, alltypes.nameSubjectEntry)
    # create text field objects
    contTextField = gui.receiveObject(builder, alltypes.nameContTextField)
    # create file chooser wigget objects
    attachFileChooserWidget = gui.receiveObject(builder, alltypes.nameAttachFileChooserWidget)
    # create label objects
    errorLabel = gui.receiveObject(builder, alltypes.nameErrorLabel)
    # create toolbar for text modification
    toolbarTextMod = gui.receiveObject(builder, alltypes.nameToolbarTextMod)
    # create new tags for text modification
    boldTag, italicTag, underlineTag = gui.createTextTags(contTextField)
    # add new buttons to toolbar object
    boldButton, italicButton, underlineButton = gui.addNewButtonsToToolbar(toolbarTextMod)
    # add handlers to visual objects
    gui.attachDestroyEvent(mainWindow, gui.handleDestroyButton)
    gui.attachClickEvent(mailButton, gui.handleSendMail)
    gui.attachClickEventArg(boldButton, gui.handleBoldButton, boldTag)
    gui.attachClickEventArg(italicButton, gui.handleItalicButton, italicTag)
    gui.attachClickEventArg(underlineButton, gui.handleUnderlineButton, underlineTag)
    # save references to file chooser objects in GUI module
    gui.saveRefToFileChooserObjects(dataFileChooser)
    # save references to text entry objects in GUI module
    gui.saveRefToTextEntryObjects(subjectEnrty)
    # save references to file chooser widget objects in GUI module
    gui.saveRefToFileChooserWidgetObjects(attachFileChooserWidget)
    # save references to label objects in GUI module
    gui.saveRefToLabelObjects(errorLabel)
    # save references to text field objects in GUI module
    gui.saveRefToTexfFieldObjects(contTextField)
    # save references to toolbar objects in GUI module
    gui.saveRefToToolbarObjects(toolbarTextMod)
    # initialize visual objects for GUI
    gui.initializeVisualObjects()
    # launch graphical interface
    gui.launchInterface(mainWindow)

# specify entry point
if __name__ == "__main__":
    main()
