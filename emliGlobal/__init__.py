"""
This is EmLi package for sending mails for several users.
The given file is default for Python project and has some basic
documentation.
"""
from .emli import main
from .alltypes import *
from .gui import *
from .reader import *
from .mail import *
